#!/bin/bash

# Read secrets from pass and set as environment variables
export TF_VAR_username=$(pass Terraform/terraform-prov@pve)
export TF_VAR_password=$(pass Terraform/terraform-prov@pve!terraform-token)

# When you run Terraform, it'll pick up the secrets automatically
terraform apply
