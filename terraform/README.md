# Scriptorium Homelab Terraform Setup

## Initial Config

1. Add role

```bash
pveum role add terraform-role -privs "VM.Allocate VM.Clone VM.Config.CDROM VM.Config.CPU VM.Config.Cloudinit VM.Config.Disk VM.Config.HWType VM.Config.Memory VM.Config.Network VM.Config.Options VM.Monitor VM.Audit VM.PowerMgmt Datastore.AllocateSpace Datastore.Audit"
```

2. Add user to role

```bash
pveum aclmod / -user terraform-prov@pve -role terraform-role
```

3. Add user password and API token

```bash
# Add password
pveum user add terraform-prov@pve --password <password>
# Add API token
pveum user token add terraform-prov@pve terraform-token --privsep=0
```

4. For the local environment, secrets will be stored in `gnu-pass`:

```bash
# Initialize Pass password store
pass init <gnupg key name>

# Insert the two terraform secrets
pass insert Terraform/terraform-prov@pve
pass insert Terraform/terraform-prov@pve!terraform-token

# Read secrets from pass and set as environment variables
export TF_VAR_username=$(pass Terraform/terraform-prov@pve)
export TF_VAR_password=$(pass Terraform/terraform-prov@pve!terraform-token)

# When you run Terraform, it'll pick up the secrets automatically
terraform apply
```
