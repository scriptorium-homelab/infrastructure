# CI/CD info for Ansible Playbooks

## Pipeline Stages

There are two stages in the pipeline:

1. **config:** This stage involves necessary setup for running Ansible. It includes creating the `.ssh` directory, setting up SSH keys, and preparing the Ansible Vault password file.
2. **run:** During this stage, an Ansible playbook is run against specified hosts.

## Jobs

### Node Exporter:

- **node_exporter_playbook:** This job runs the `node_exporter.yml` Ansible playbook against the host specified by the `HOST` variable. The playbook is responsible for installing and configuring the Prometheus Node Exporter that provides detailed system metrics for use by the Prometheus monitoring system.

## Running Jobs

To run a job in the pipeline:

1. Navigate to the CI/CD > Pipelines section of the project in GitLab.
2. Click the "Run Pipeline" button.
3. Select the branch you want to run the pipeline on.
4. In the "Run for" dropdown, select the name of the job you want to run (e.g., `node_exporter_playbook`).
5. In the "Variables" section, set any required variables for the job. For the `node_exporter_playbook` job, you need to set the `HOST` variable to the host you want to run the playbook on, and the `PLAYBOOK` variable to the path of the playbook you want to run.
6. Click the "Run Pipeline" button to start the pipeline.

> Note: These jobs are designed to be run on the `main` branch as specified in the `rules` section of the job. Modify this as necessary based on your Git branching model.

## Initial Local Usage

To run the initial host playbook, use the following command:

```bash
ansible-playbook -v -i hosts playbooks/cloister_config.yml -K --ask-vault-pass
```
