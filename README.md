> <p align="center">
>   <img src="diagrams/initial-concept.jpg" alt="Homelab concept diagram">
> </p>

# Scriptorium Homelab

## Overview

The homelab setup consists of a Intel NUC central server `inner-cloister` connected to both a home router and acts as a router to grant internet access via its wifi interface to a switch with four Dell Wyse 3040 thin clients. It also acts as a K3s master, and the other three are K3s workers. Each worker is equipped with a 32 GB flash drive, intended to be set up as a MinIO cluster for object storage.

### Intel NUC8i3BEH specs

| Component | Specification                                                                                                                          |
| --------- | -------------------------------------------------------------------------------------------------------------------------------------- |
| Processor | Intel Core i3-8109U Dual-Core Processor (4MB Cache, up to 3.60GHz)                                                                     |
| RAM       | 2x DDR4 SO-DIMM slots 32GB (2400 MHz)                                                                                                  |
| Storage   | 500 Gb M.2 NVMe and 2.5" SATA3 1 TB SSD                                                                                                |
| Graphics  | Intel Iris Plus Graphics 655                                                                                                           |
| Ports     | 1x HDMI 2.0a, 1x Thunderbolt 3 (Type-C), 4x USB 3.0 (including one charging port), 1x Mini DisplayPort 1.2, 1x Gigabit Ethernet (RJ45) |

### Wyse client specs

| Component | Specification                                                      |
| --------- | ------------------------------------------------------------------ |
| Processor | Intel Atom x5 Z8350 Quad-Core Processor (2MB Cache, up to 1.92GHz) |
| RAM       | 2GB (DDR3)                                                         |
| Storage   | 8GB (Flash) and 32 Gb flash drive                                  |
| Graphics  | Intel HD Graphics                                                  |
| Ports     | 1x DisplayPort, 1x HDMI, 3x USB 2.0, 1x USB 3.0, 1x RJ45           |

## Bare Metal Infrastructure List

### inner-cloister

The host machine is an Intel NUC8i3BEH, responsible for running a few integral services:

- [x] **Networking Services**: Depending on networking needs, DHCP, DNS, or VPN servers might be set up:
  - [x] **Routing**: Routes traffic to k3s workers.
  - [x] **DHCP**: The host assigns static IPs to the k3s hosts
- [x] **GitLab Runner**: Handles the CI/CD tasks for the whole Infrastructure, primarily for initial host configuration via ansible, docker image builde and run jobs for the K3s cluster and communicating with its Gitlab agent. The runner is configured directly on the machine with the initial `vm_host_config.yml` ansible playbook, along with all dependencies.

### cloister

A Dell Wyse client on the same network

### magister

A Dell Wyse client on the same network

## Kubernetes List

The K3s cluster, hosted on the 3 Dell Wyse 3040 thin clients and main host machine, is intended for running containerized applications in a lightweight Kubernetes environment. It consists of one K3s master and three K3s workers. Config repo is found here: https://gitlab.com/scriptorium-homelab/clusters/k3s-local

- [x] **Gitlab Agent**: A Gitlab agent is registered to handle deployments via helm.
- [x] **Monitoring and logging**: Monitoring is handled by standard stack of Prometheus, Alertmanager, Grafana and Loki/Promtail.

## Storage

- [ ] **Object/Persisant Storage**: In the works. Needed for kubernetes persistant storage, backups, etc.
- [ ] **Artifact strorage**: In the works. Needed for container images, helm packages, etc.
